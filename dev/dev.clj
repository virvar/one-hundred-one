(ns dev
  (:require [figwheel.main.api :as figwheel]
            [figwheel-options]
            [one-hundred-one.server :as server]))

(defonce figwheel-build (atom nil))

(defn start []
  (server/start {:port 3000 :join? false}))

(defn stop []
  (server/stop))

(defn restart []
  (stop)
  (start))

(defn run-figwheel []
  (figwheel/stop-all)
  (when-let [config figwheel-options/config]
    (figwheel/start {:mode :serve} config)
    (reset! figwheel-build (:id config))
    nil))

(defn stop-figwheel []
  (figwheel/stop-all))

(defn cljs-repl []
  (figwheel/cljs-repl @figwheel-build))
