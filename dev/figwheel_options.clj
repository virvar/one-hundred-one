(ns figwheel-options)

(def config
  {:id      "main"
   :options {:main                 'one-hundred-one.core
             :output-to            "resources/public/js/app.js"
             :output-dir           "resources/public/js/out"
             :asset-path           "/js/out"
             :source-map-timestamp true
             :preloads             ['devtools.preload]
             :external-config      {:devtools/config {:features-to-install :all}}}
   :config  {:css-dirs   ["resources/public/css"]
             :log-file   "logs/figwheel.log"
             :watch-dirs ["src/cljs"]
             :open-url   false}})
