(ns ^:figwheel-hooks one-hundred-one.core
  (:require [reagent.dom :as dom]
            [re-frame.core :as r]
            [one-hundred-one.events]
            [one-hundred-one.subs]
            [one-hundred-one.views :as views]
            [one-hundred-one.config :as config]))

(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn- ^:after-load mount-root []
  (r/clear-subscription-cache!)
  (dom/render [views/main-panel]
              (js/document.getElementById "app")))

(defn ^:export init []
  (r/dispatch-sync [:initialize-db])
  (dev-setup)
  (mount-root))
