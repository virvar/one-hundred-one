(ns one-hundred-one.websocket
  (:require [cognitect.transit :as t]))

(defonce ws-chan (atom nil))
(def json-reader (t/reader :json))
(def json-writer (t/writer :json))


(defn receive-transit-msg! [update-fn]
  (fn [msg]
    (update-fn
     (t/read json-reader (.-data msg)))))

(defn send-transit-msg! [msg]
  (if @ws-chan
    (.send @ws-chan (t/write json-writer msg))
    (throw (js/Error. "Websocket is not available!"))))

(defn close! []
  (when @ws-chan
    (.close @ws-chan)))

(defn make-websocket! [url open-handler receive-handler]
  (close!)
  (if-let [chan (js/WebSocket. url)]
    (do
      (set! (.-onopen chan) open-handler)
      (set! (.-onmessage chan) (receive-transit-msg! receive-handler))
      (reset! ws-chan chan))
    (throw (js/Error. "Websocket connection failed!"))))
