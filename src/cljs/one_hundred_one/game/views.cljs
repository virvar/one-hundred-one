(ns one-hundred-one.game.views
  (:require [re-frame.core :as r]))

(def card-width 112)
(def card-height 156)
(def bottom-card-width 23)

(defn- get-card-key [{:keys [suit rank]}]
  (+ (* suit 14) rank))

(defn- get-card-offset [position]
  (str (* bottom-card-width position) "px"))

(defn- open-card-template [{:keys [suit rank]}]
  [:div.card-base.open-card
   {:style {:background-position (str (- (* rank card-width)) "px "
                                      (- (* suit card-height)) "px")}}])

(defn- enemy-view [enemy current?]
  ^{:key (:user-id enemy)}
  [:div.enemy
   [:div.hand.enemy-hand
    (for [position (range (:cards-count enemy))]
      ^{:key position}
      [:div.hand-card
       {:style {:left (get-card-offset position)}}
       [:div.card-base.closed-card]])]
   [:div.enemy-info
    (str (if current? "> " "") (:name enemy) " " (:score enemy))]])

(defn- enemies-view []
  (let [players @(r/subscribe [:players])
        enemies @(r/subscribe [:enemies])
        current-player-index @(r/subscribe [:current-player-index])
        current-player-id (get-in players [current-player-index :user-id])]
    [:div#enemies
     (map (fn [enemy]
            (enemy-view enemy (= (:user-id enemy) current-player-id)))
          enemies)]))

(defn- player-hand-card-template [position card]
  ^{:key (get-card-key card)}
  [:div.hand-card
   {:class    [(when (:available? card) "available-card")
               (when (:selected? card) "selected-card")]
    :style    {:left (get-card-offset position)}
    :on-click (when (:available? card)
                #(r/dispatch [:select-card position]))}
   [open-card-template card]])

(defn- suit-template [suit]
  [:div.suit
   {:style {:background-position (str (- (* suit 40)) "px 0")}}])

(defn- suit-option-template [position suit selected?]
  ^{:key suit}
  [:div.suit-option
   {:class    (when selected? "selected-card")
    :style    {:background-position (str (- (* suit 40)) "px 0")
               :left                (get-card-offset position)}
    :on-click #(r/dispatch [:select-suit suit])}
   [suit-template suit]])

(defn- player-view []
  (let [player @(r/subscribe [:player])
        player-turn? @(r/subscribe [:player-turn?])
        hand @(r/subscribe [:hand])]
    [:<>
     [:div#my-hand.hand
      (map-indexed player-hand-card-template hand)]
     [:div#suits
      (when (:select-suit? player)
        (map-indexed (fn [pos suit]
                       (suit-option-template pos suit (= suit (:selected-suit player))))
                     (range 4)))]
     [:div
      (str (if player-turn? "> " "") (:name player) " " (:score player))]]))

(defn- card-in-game-template [position card]
  ^{:key position}
  [:div.hand-card
   {:style {:left (get-card-offset position)}}
   [open-card-template card]])

(defn- selected-suit-view []
  (when-let [selected-suit @(r/subscribe [:selected-suit])]
    [suit-template selected-suit]))

(defn- cards-in-game-view []
  (let [cards @(r/subscribe [:open-cards])]
    [:div.hand
     (map-indexed card-in-game-template (reverse cards))]))

(defn- game-over-view []
  (let [game-over? @(r/subscribe [:game-over?])
        winner @(r/subscribe [:winner])]
    (when game-over?
      [:div [:span (:name winner)] "wins"])))

(defn game-view []
  (let [can-turn? @(r/subscribe [:can-turn?])
        can-take-card? @(r/subscribe [:can-take-card?])
        can-skip? @(r/subscribe [:can-skip?])
        auto-skip? @(r/subscribe [:-> :auto-skip?])
        auto-take-card? @(r/subscribe [:-> :auto-take-card?])
        auto-select-card? @(r/subscribe [:-> :auto-select-card?])]
    [:div
     [game-over-view]
     [enemies-view]
     [cards-in-game-view]
     [selected-suit-view]
     [player-view]
     [:div.row.row-cols-lg-auto.align-items-center.g-2.pt-2
      [:div.col
       [:button.btn.btn-outline-secondary
        {:disabled (not can-turn?)
         :on-click #(r/dispatch [:turn])}
        "Apply"]]
      [:div.col
       [:button.btn.btn-outline-secondary
        {:disabled (not can-take-card?)
         :on-click #(r/dispatch [:take-card])}
        "Take a card"]]
      [:div.col
       [:button.btn.btn-outline-secondary
        {:disabled (not can-skip?)
         :on-click #(r/dispatch [:skip])}
        "Skip"]]]
     [:div.row.row-cols-lg-auto.align-items-center.g-2.pt-2
      (let [check-id "input-auto-skip"]
        [:div.col
         [:div.form-check
          [:input.form-check-input
           {:id        check-id
            :type      "checkbox"
            :value     (or auto-skip? false)
            :on-change (fn [e]
                         (let [value (.. e -target -checked)]
                           (r/dispatch [:set :auto-skip? value])))}]
          [:label.form-check-label
           {:for check-id} "Auto skip"]]])
      (let [check-id "input-auto-take-card"]
        [:div.col
         [:div.form-check
          [:input.form-check-input
           {:id        check-id
            :type      "checkbox"
            :value     (or auto-take-card? false)
            :on-change (fn [e]
                         (let [value (.. e -target -checked)]
                           (r/dispatch [:set :auto-take-card? value])))}]
          [:label.form-check-label
           {:for check-id} "Auto auto take card"]]])
      (let [check-id "input-auto-select-card"]
        [:div.col
         [:div.form-check
          [:input.form-check-input
           {:id        check-id
            :type      "checkbox"
            :value     (or auto-select-card? false)
            :on-change (fn [e]
                         (let [value (.. e -target -checked)]
                           (r/dispatch [:set :auto-select-card? value])))}]
          [:label.form-check-label
           {:for check-id} "Auto auto select card"]]])]]))
