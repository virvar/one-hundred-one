(ns one-hundred-one.game.events
  (:require [re-frame.core :as r]
            [one-hundred-one.client :as client]
            [one-hundred-one.game.subs :as subs]))

(def your-turn-sound (js/Audio. "audio/your_turn.mp3"))

(defn- check-game-over [db]
  (when (get-in db [:game :game-over?])
    (let [winner (apply min-key :score (get-in db [:game :players]))]
      (js/window.alert (str (:name winner) " wins")))))

(defn- new-round? [db new-db]
  (or (not= (get-in db [:game :round])
            (get-in new-db [:game :round]))
      (not= (get-in db [:game :id])
            (get-in new-db [:game :id]))))

(defn- check-new-round [db new-db]
  (when (new-round? db new-db)
    (js/window.alert (str "Round " (get-in new-db [:game :round])))))

(defn- check-your-turn [db new-db]
  (let [player-id (:user-id (get-in db [:game :player]))
        prev-player-index (get-in db [:game :current-player-index])
        current-player-index (get-in new-db [:game :current-player-index])]
    (when (and (or (not= player-id
                         (:user-id (get-in db [:game :players prev-player-index])))
                   (new-round? db new-db))
               (= player-id
                  (:user-id (get-in db [:game :players current-player-index])))
               (not (get-in new-db [:game :game-over?])))
      (.play your-turn-sound))))


(r/reg-event-fx
 :set-game
 (fn [{:keys [db]} [_ data]]
   (check-game-over data)
   (check-new-round db data)
   (check-your-turn db data)
   (let [db (-> db
                (assoc :game (-> (:game data)
                                 (update-in [:player :hand] (comp vec reverse))))
                (assoc :page :game))
         hand (subs/hand db)
         available-position (-> (keep-indexed (fn [index card]
                                                (when (:available? card)
                                                  index))
                                              hand)
                                first)]
     {:db db
      :fx (cond-> []
                  (and (:auto-skip? db)
                       (not available-position)
                       (not (subs/can-take-card? db))
                       (subs/can-skip? db))
                  (conj [:dispatch [:skip]])

                  (and (:auto-take-card? db)
                       (not available-position)
                       (subs/can-take-card? db)
                       (not (subs/can-skip? db)))
                  (conj [:dispatch [:take-card]])

                  (and (:auto-select-card? db)
                       available-position)
                  (conj [:dispatch [:select-card available-position]]))})))

(r/reg-event-db
 :select-card
 (fn [db [_ position]]
   (let [card (get-in db [:game :player :hand position])]
     (update-in db [:game :player] (fn [player]
                                     (assoc player :select-suit? (= 11 (:rank card))
                                                   :selected-card-index position))))))

(r/reg-event-db
 :select-suit
 (fn [db [_ suit]]
   (update-in db [:game :player] assoc :selected-suit suit)))

(r/reg-event-db
 :take-card
 (fn [db _]
   (client/take-card!)
   db))

(r/reg-event-db
 :skip
 (fn [db _]
   (client/skip!)
   db))

(r/reg-event-db
 :turn
 (fn [db _]
   (client/turn! db)
   db))
