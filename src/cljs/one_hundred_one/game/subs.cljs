(ns one-hundred-one.game.subs
  (:require [re-frame.core :as r]))

(defn- player-turn? [db]
  (let [current-player-index (get-in db [:game :current-player-index])]
    (and (= (:user-id (get-in db [:game :player]))
            (:user-id (get-in db [:game :players current-player-index])))
         (not (get-in db [:game :game-over?])))))

(defn- can-turn-with-card? [game card]
  (let [top-card (first (:open-cards game))
        current-suit (or (:selected-suit game) (:suit top-card))
        player-last-card (last (get-in game [:player :hand]))]
    (and (or (and (nil? top-card) (= card player-last-card))
             (= (:rank top-card) (:rank card))
             (= current-suit (:suit card)))
         (or (not (:card-taken? game))
             (= card player-last-card)))))

(defn- can-turn? [db]
  (when (player-turn? db)
    (let [player (get-in db [:game :player])
          card-index (:selected-card-index player)]
      (when card-index
        (let [card (get-in player [:hand card-index])]
          (can-turn-with-card? (:game db) card))))))

(defn can-take-card? [db]
  (when (player-turn? db)
    (let [game (:game db)]
      (and (first (:open-cards game))
           (or (:must-play-card? game)
               (not (:card-taken? game)))))))

(defn can-skip? [db]
  (and (player-turn? db)
       (not (can-take-card? db))))

(defn hand [db]
  (let [player-turn? (player-turn? db)
        selected-index (get-in db [:game :player :selected-card-index])]
    (map-indexed (fn [pos card]
                   (if player-turn?
                     (-> card
                         (assoc :available? (can-turn-with-card? (:game db) card))
                         (assoc :selected? (= selected-index pos)))
                     card))
                 (get-in db [:game :player :hand]))))

(r/reg-sub
 :game-over?
 (fn [db]
   (get-in db [:game :game-over?])))

(r/reg-sub
 :winner
 (fn [db]
   (apply min-key :score (get-in db [:game :players]))))

(r/reg-sub
 :players
 (fn [db]
   (get-in db [:game :players])))

(r/reg-sub
 :enemies
 (fn [db]
   (let [user-id (get-in db [:game :player :user-id])]
     (remove #(= user-id (:user-id %)) (get-in db [:game :players])))))

(r/reg-sub
 :player
 (fn [db]
   (get-in db [:game :player])))

(r/reg-sub
 :current-player-index
 (fn [db]
   (get-in db [:game :current-player-index])))

(r/reg-sub
 :player-turn?
 player-turn?)

(r/reg-sub
 :can-turn?
 can-turn?)

(r/reg-sub
 :can-take-card?
 can-take-card?)

(r/reg-sub
 :can-skip?
 can-skip?)

(r/reg-sub
 :open-cards
 (fn [db]
   (get-in db [:game :open-cards])))

(r/reg-sub
 :hand
 (fn [db]
   (hand db)))

(r/reg-sub
 :selected-suit
 (fn [db]
   (get-in db [:game :selected-suit])))
