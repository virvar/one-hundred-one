(ns one-hundred-one.client
  (:require [re-frame.core :as r]
            [one-hundred-one.websocket :as ws]))

(defn take-card! []
  (ws/send-transit-msg! {:method :take-card}))

(defn skip! []
  (ws/send-transit-msg! {:method :skip}))

(defn turn! [db]
  (let [player (get-in db [:game :player])
        card-index (:selected-card-index player)
        card (get-in player [:hand card-index])]
    (when card
      (ws/send-transit-msg! {:method        :turn
                             :card          card
                             :selected-suit (:selected-suit player)}))))

(defn process-message! [data]
  (case (:method data)
    :new-game-update (r/dispatch [:set-room data])
    :start-game (r/dispatch [:set-game data])
    :update-game (r/dispatch [:set-game data])
    (.log js/console data "! Unhandled message")))

(defn connect! [db]
  (.log js/console "connecting...")
  (ws/make-websocket! (str "ws://" (.. js/window -location -host) "/ws")
                      (fn [data]
                        (.log js/console data)
                        (ws/send-transit-msg! {:method   :set-username
                                               :username (get-in db [:settings :username])}))
                      (fn [data]
                        (process-message! data))))

(defn start-game! []
  (ws/send-transit-msg! {:method :start-game}))
