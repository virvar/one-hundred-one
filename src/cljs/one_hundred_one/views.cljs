(ns one-hundred-one.views
  (:require [re-frame.core :as r]
            [one-hundred-one.room.views :as room]
            [one-hundred-one.game.views :as game]))

(defn main-panel []
  (let [page @(r/subscribe [:-> :page])]
    [:div.container-fluid
     [room/connection-view]
     (case page
       :room [room/room-view]
       :game [game/game-view]
       nil)]))
