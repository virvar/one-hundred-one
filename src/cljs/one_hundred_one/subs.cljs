(ns one-hundred-one.subs
  (:require [re-frame.core :as r]
            [one-hundred-one.room.subs]
            [one-hundred-one.game.subs]))

(r/reg-sub
 :->
 (fn [db [_ kw]]
   (if (vector? kw)
     (get-in db kw)
     (kw db))))
