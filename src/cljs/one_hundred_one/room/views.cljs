(ns one-hundred-one.room.views
  (:require [re-frame.core :as r]))

(defn- players-view []
  (let [players @(r/subscribe [:room-players])]
    [:div
     (for [player players]
       ^{:key (:user-id player)}
       [:li (:name player)])]))

(defn room-view []
  [:div
   [players-view]
   [:button.btn.btn-outline-secondary
    {:disabled true
     ;:on-click #(dispatch [:start-game])
     }
    "Start"]])

(defn connection-view []
  (let [input-id "input-username"]
    [:div.row.row-cols-lg-auto.align-items-center.g-2.pt-2
     [:div.col
      [:label {:for input-id} "Name:"]]
     [:div.col
      [:input
       {:id        input-id
        :class     "form-control"
        :type      "text"
        :on-change (fn [e]
                     (r/dispatch [:set-username (.. e -target -value)]))}]]
     [:div.col
      [:button.btn.btn-outline-secondary
       {:on-click #(r/dispatch [:connect])}
       "Connect"]]]))
