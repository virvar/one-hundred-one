(ns one-hundred-one.room.events
  (:require [re-frame.core :as r]
            [one-hundred-one.client :as client]))

(r/reg-event-db
 :set-username
 (fn [db [_ username]]
   (assoc-in db [:settings :username] username)))

(r/reg-event-db
 :connect
 (fn [db _]
   (client/connect! db)
   db))

(r/reg-event-db
 :set-room
 (fn [db [_ data]]
   (assoc db :room (:game data)
             :page :room)))

(r/reg-event-db
 :start-game
 (fn [db _]
   (client/start-game!)
   db))
