(ns one-hundred-one.room.subs
  (:require [re-frame.core :as r]))

(r/reg-sub
 :room-players
 (fn [db]
   (get-in db [:room :players])))
