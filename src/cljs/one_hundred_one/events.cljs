(ns one-hundred-one.events
  (:require [re-frame.core :as r]
            [one-hundred-one.room.events]
            [one-hundred-one.game.events]))

(def db-state (atom nil))

(r/reg-event-db
 :initialize-db
 (fn [_ _] {}))

(r/reg-event-db
 :set
 (fn [db [_ kw v]]
   (if (vector? kw)
     (assoc-in db kw v)
     (assoc db kw v))))

(r/reg-event-db
 :del
 (fn [db [_ & ks]]
   (apply dissoc db ks)))

(r/reg-event-db
 :conj
 (fn [db [_ ks value & [type]]]
   (update-in db ks
     (if (= type :vector)
       #(conj (vec %) value)
       #(conj (set %) value)))))

(r/reg-event-db
 :db-snapshot
 (fn [db _]
   (reset! db-state db)
   db))
