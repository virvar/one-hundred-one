(ns one-hundred-one.logic
  (:require [one-hundred-one.client :refer [send-data!]]
            [one-hundred-one.game :as game]))

(defonce db (atom {:games    {}
                   :users    {}
                   :new-game {:id      1
                              :round   0
                              :players []}}))


(defn status []
  (let [state @db]
    {:users (count (:users state))
     :games (count (:games state))}))

(defn send-game! [db method game]
  (let [game-model (-> game
                       (dissoc :deck)
                       (update :players
                               (partial mapv (fn [player]
                                               (-> player
                                                   (assoc :cards-count (count (:hand player)))
                                                   (dissoc :hand))))))]
    (doseq [player (:players game)
            :let [user (get-in db [:users (:user-id player)])]]
      (send-data! (:channel user) {:method method
                                   :game   (assoc game-model :player player)}))))

(defn remove-game-if-empty [db game-id]
  (if (= 0 (count (get-in db [:games game-id :players])))
    (update db :games dissoc game-id)
    db))

(defn remove-player [db user-id]
  (let [game-id (get-in db [:users user-id :game-id])
        new-game-player? (= (get-in db [:new-game :id]) game-id)
        game-path (if new-game-player?
                    [:new-game]
                    [:games game-id])]
    (-> db
        (update-in game-path game/remove-player-from-game user-id)
        (remove-game-if-empty game-id)
        (update :users dissoc user-id))))

(defn remove-player! [user-id]
  (let [state (swap! db remove-player user-id)]
    ;; todo: works only for :new-game players
    (send-game! state :new-game-update (:new-game state))))

(defn take-card [db user-id]
  (let [game-id (get-in db [:users user-id :game-id])]
    (update-in db [:games game-id] (fn [game]
                                     (if (game/user-turn? game user-id)
                                       (game/take-card game)
                                       game)))))

(defn take-card! [user-id]
  (let [prev-state @db
        state (swap! db take-card user-id)
        game-id (get-in state [:users user-id :game-id])
        game (get-in state [:games game-id])]
    (when (not= state prev-state)
      (send-game! state :update-game game))))

(defn skip [db user-id]
  (let [game-id (get-in db [:users user-id :game-id])]
    (update-in db [:games game-id] (fn [game]
                                     (if (game/user-turn? game user-id)
                                       (game/skip game)
                                       game)))))

(defn skip! [user-id]
  (let [prev-state @db
        state (swap! db skip user-id)
        game-id (get-in state [:users user-id :game-id])
        game (get-in state [:games game-id])]
    (when (not= state prev-state)
      (send-game! state :update-game game))))

(defn turn [db user-id card selected-suit]
  (let [game-id (get-in db [:users user-id :game-id])]
    (update-in db [:games game-id] (fn [game]
                                     (if (game/user-turn? game user-id)
                                       (game/turn game card selected-suit)
                                       game)))))

(defn turn! [user-id card selected-suit]
  (let [prev-state @db
        state (swap! db turn user-id card selected-suit)
        game-id (get-in state [:users user-id :game-id])
        game (get-in state [:games game-id])]
    (when (not= state prev-state)
      (send-game! state :update-game game))))

(defn start-game [db]
  (let [game (-> (:new-game db)
                 (update :players #(vec (shuffle %)))
                 game/new-round)]
    (-> db
        (update :games assoc (:id game) game)
        (update :new-game (fn [game]
                            (-> game
                                (update :id inc)
                                (assoc :players [])))))))

(defn start-game-if-ready [db]
  (if (= 2 (count (get-in db [:new-game :players])))
    (start-game db)
    db))

(defn add-player [db user]
  (-> db
      (update-in [:new-game :players] conj {:user-id (:id user)
                                            :name    (:name user)
                                            :score   0})
      (update :users assoc (:id user) (assoc user
                                        :game-id (get-in db [:new-game :id])))
      start-game-if-ready))

(defn add-player! [user]
  (let [state (swap! db add-player user)
        game-id (get-in state [:users (:id user) :game-id])
        new-game-player? (= (get-in state [:new-game :id]) game-id)]
    (if new-game-player?
      (send-game! state :new-game-update (:new-game state))
      (send-game! state :start-game (get-in state [:games game-id])))))
