(ns one-hundred-one.core
  (:require [config.core :refer [env]]
            [one-hundred-one.server :as server])
  (:gen-class))

(defn -main [& args]
  (let [port (Integer/parseInt (or (env :port) "3000"))]
    (server/start {:port port :join? false})))
