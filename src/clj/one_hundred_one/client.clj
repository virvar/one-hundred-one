(ns one-hundred-one.client
  (:require [org.httpkit.server :refer [send!]]
            [cognitect.transit :as transit])
  (:import [java.io ByteArrayInputStream ByteArrayOutputStream]))

(def charset "UTF-8")
(def out (ByteArrayOutputStream. 4096))
(def writer (transit/writer out :json))

(defn send-data! [channel data]
  (.reset out)
  (transit/write writer data)
  (send! channel
         (.toString out charset)))

(defn read-data [msg]
  (let [in (ByteArrayInputStream. (.getBytes msg charset))
        reader (transit/reader in :json)]
    (transit/read reader)))
