(ns one-hundred-one.server
  (:require [one-hundred-one.handler :refer [handler]]
            [org.httpkit.server :refer [run-server]]))

(defonce running-server (atom nil))

(defn start [http-config]
  (reset! running-server (run-server handler http-config))
  (println "Listening on port " (:port http-config)))

(defn stop []
  (when-let [server @running-server]
    (server)
    (println "Server stopped")))
