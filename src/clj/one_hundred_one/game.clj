(ns one-hundred-one.game)

(def hand-size 4)
(def deck
  (for [suit (range 4)
        rank (conj (range 5 13) 0)]
    {:suit suit, :rank rank}))
(def suits {:clubs    0
            :diamonds 1
            :hearts   2
            :spades   3})
(def cards-costs {0  11
                  5  6
                  6  7
                  7  8
                  8  9
                  9  10
                  10 2
                  11 3
                  12 4})


(defn remove-player-from-game [game user-id]
  (update game :players (partial remove #(= (:user-id %) user-id))))

(defn user-turn? [game user-id]
  (= user-id (:user-id (get-in game [:players (:current-player-index game)]))))

(defn get-current-player-index [game]
  (.indexOf (:players game) (first (filter #(empty? (:hand %)) (:players game)))))

(defn get-next-player-index [game current-player-index]
  (rem (inc current-player-index) (count (:players game))))

(defn check-deck-size [game]
  (if (< 0 (count (:deck game)))
    game
    (-> game
        (assoc :deck (reverse (rest (:open-cards game))))
        (assoc :open-cards (list (first (:open-cards game)))))))

(defn take-card-for [game player-index]
  (as-> game g
        (check-deck-size g)
        (update-in g [:players player-index]
                   (fn [player]
                     (if (not (empty? (:deck g)))
                       (update player :hand conj (first (:deck g)))
                       player)))
        (update g :deck rest)))

(defn take-cards-for [game player-index cards-count]
  (nth (iterate #(take-card-for % player-index) game) cards-count))

(defn take-card [game]
  (if (and (first (:open-cards game))
           (or (:must-play-card? game)
               (not (:card-taken? game))))
    (-> game
        (take-card-for (:current-player-index game))
        (assoc :card-taken? true))
    game))

(defn skip [game]
  (if (:card-taken? game)
    (as-> game g
          (assoc g :card-taken? false)
          (if (:must-play-card? g)
            g
            (update g :current-player-index (partial get-next-player-index g))))
    game))

(defn game-over? [game]
  (some (fn [player]
          (< 101 (:score player)))
        (:players game)))

(declare new-round)

(defn check-game-over [game]
  (if (game-over? game)
    (assoc game :game-over? true)
    (new-round game)))

(defn calculate-player-score [player]
  (update player :score
          (fn [score]
            (let [round-score (reduce + (map #(get cards-costs (:rank %)) (:hand player)))
                  new-score (+ score round-score)]
              (if (= 101 new-score)
                0
                new-score)))))

(defn round-end? [game]
  (some (fn [player]
          (and (empty? (:hand player))
               (not (:must-play-card? game))))
        (:players game)))

(defn check-round-end [game]
  (if (round-end? game)
    (-> game
        (update :players (partial mapv calculate-player-score))
        check-game-over)
    game))

(defn can-turn? [game card selected-suit]
  (let [top-card (first (:open-cards game))
        current-suit (or (:selected-suit game) (:suit top-card))
        player-hand (get-in game [:players (:current-player-index game) :hand])
        player-last-card (first player-hand)
        player-has-card? (some #{card} player-hand)]
    (when-not player-has-card?
      (println "Player doesn't have the card: " card ", hand: " player-hand))
    (and player-has-card?
         (or (and (nil? top-card) (= card player-last-card))
             (= (:rank top-card) (:rank card))
             (= current-suit (:suit card)))
         (or selected-suit
             (not= (:rank card) 11))
         (or (not (:card-taken? game))
             (= card player-last-card)))))

(defn process-card [game card selected-suit]
  (cond
    (= 0 (:rank card)) (update game :current-player-index #(get-next-player-index game (get-next-player-index game %)))
    (= 5 (:rank card)) (let [to-player-index (get-next-player-index game (:current-player-index game))
                             take-count 2]
                         (-> game
                             (take-cards-for to-player-index take-count)
                             (assoc :current-player-index (get-next-player-index game to-player-index))))
    (= 6 (:rank card)) (let [to-player-index (get-next-player-index game (:current-player-index game))
                             take-count (if (= (:spades suits) (:suit card)) 4 1)]
                         (-> game
                             (take-cards-for to-player-index take-count)
                             (assoc :current-player-index (get-next-player-index game to-player-index))))
    (= 7 (:rank card)) (assoc game :must-play-card? true)
    (= 11 (:rank card)) (-> game
                            (assoc :selected-suit selected-suit)
                            (update :current-player-index (partial get-next-player-index game)))
    :else (update game :current-player-index (partial get-next-player-index game))))

(defn turn [game card selected-suit]
  (if (can-turn? game card selected-suit)
    (-> game
        (update-in [:players (:current-player-index game)] (fn [player]
                                                             (update player :hand (partial remove (partial = card)))))
        (assoc :must-play-card? false)
        (dissoc :selected-suit)
        (assoc :card-taken? false)
        (process-card card selected-suit)
        (update :open-cards conj card)
        check-round-end)
    game))

(defn shuffle-deck []
  (shuffle deck))

(defn new-round [game]
  (let [deck (shuffle-deck)
        current-player-index (get-current-player-index game)
        first-card (nth deck (+ (* hand-size current-player-index) (dec hand-size)))]
    (-> game
        (update :round inc)
        (assoc :deck (drop (* hand-size (count (:players game))) deck))
        (update :players #(vec (map-indexed (fn [index player]
                                              (assoc player :hand (->> deck
                                                                       (drop (* hand-size index))
                                                                       (take hand-size)
                                                                       reverse)))
                                            %)))
        (assoc :open-cards ())
        (assoc :current-player-index current-player-index)
        (turn first-card nil))))
