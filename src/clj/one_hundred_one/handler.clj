(ns one-hundred-one.handler
  (:require [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [resources]]
            [ring.util.response :refer [resource-response]]
            [ring.middleware.reload :refer [wrap-reload]]
            [one-hundred-one.ws-handler :refer [ws-handler]]))

(defroutes routes
           (GET "/" [] (resource-response "index.html" {:root "public"}))
           (GET "/ws" request (ws-handler request))
           (resources "/"))

(def dev-handler (-> #'routes wrap-reload))

(def handler routes)
