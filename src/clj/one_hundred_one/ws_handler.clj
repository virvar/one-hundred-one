(ns one-hundred-one.ws-handler
  (:require [org.httpkit.server :refer [with-channel on-close on-receive]]
            [one-hundred-one.client :refer [read-data send-data!]]
            [one-hundred-one.logic :as logic]))

(defonce channels-users (atom {}))
(defonce last-user-id (atom 0))


(defn process-message! [channel data]
  (case (:method data)
    :set-username (logic/add-player! {:id (get @channels-users channel), :name (:username data), :channel channel})
    :take-card (logic/take-card! (get @channels-users channel))
    :skip (logic/skip! (get @channels-users channel))
    :turn (logic/turn! (get @channels-users channel) (:card data) (:selected-suit data))
    (println "! Unhandled message")))

(defn connect! [channel]
  (let [user-id (swap! last-user-id inc)]
    (swap! channels-users assoc channel user-id)
    (println "User connected, channels:" (count @channels-users))))

(defn disconnect! [channel status]
  (logic/remove-player! (get @channels-users channel))
  (swap! channels-users dissoc channel)
  (println (str "User disconnected (" status
                "), channels: " (count @channels-users)
                ", " (logic/status))))

(defn ws-handler [request]
  (with-channel
    request channel
    (connect! channel)
    (on-close channel (partial disconnect! channel))
    (on-receive channel (fn [msg]
                          (process-message! channel (read-data msg))))))
