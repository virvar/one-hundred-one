(defproject one-hundred-one "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [compojure "1.6.3"]
                 [yogthos/config "1.2.0"]
                 [ring/ring-core "1.9.5"]
                 [ring/ring-jetty-adapter "1.9.5"]
                 [ring "1.9.5"]
                 [http-kit "2.6.0"]
                 [com.cognitect/transit-clj "1.0.329"]

                 [org.clojure/clojurescript "1.11.60"]
                 [cljsjs/react "17.0.2-0"]
                 [cljsjs/react-dom "17.0.2-0"]
                 [reagent "1.1.1"]
                 [re-frame "1.3.0-rc3"]
                 [cljs-http "0.1.46"]
                 [com.cognitect/transit-cljs "0.8.280"]]

  :plugins [[lein-cljsbuild "1.1.8"]]

  :min-lein-version "2.5.3"

  :source-paths ["src/clj"]

  :clean-targets ^{:protect false} ["resources/public/js" "target"]

  :figwheel {:css-dirs     ["resources/public/css"]
             :ring-handler one-hundred-one.handler/dev-handler}

  :profiles
  {:dev  {:main         user
          :jvm-opts     ["-Xms512m" "-Xmx1536m" "-Dfile.encoding=UTF-8"]
          :source-paths ["dev"
                         "src/cljs"]
          :dependencies [[com.bhauman/figwheel-main "0.2.18"]
                         [com.bhauman/rebel-readline-cljs "0.1.4"]
                         [cider/piggieback "0.5.3"]
                         [binaryage/devtools "1.0.6"]]
          :repl-options {:nrepl-middleware [cider.piggieback/wrap-cljs-repl]
                         :timeout          120000}
          :env          {}}
   :prod {}}

  :cljsbuild
  {:builds
   [{:id           "min"
     :source-paths ["src/cljs"]
     :jar          true
     :compiler     {:output-to       "resources/public/js/app.js"
                    :optimizations   :advanced
                    :infer-externs   true
                    :closure-defines {goog.DEBUG false}
                    :pretty-print    false}}]}

  :main one-hundred-one.core
  :aot [one-hundred-one.core]
  :uberjar-name "one-hundred-one.jar"
  ;:prep-tasks [["cljsbuild" "once" "min"] "compile"]
  )
